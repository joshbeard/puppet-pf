class pf (
  $template = undef,
  $pfctl    = '/sbin/pfctl',
  $tmpfile  = '/tmp/pf.conf',
  $conf     = '/etc/pf.conf',
){

  if $template {
    file { $tmpfile:
      owner   => '0',
      group   => '0',
      mode    => '0600',
      content => template($template),
      notify  => Exec['pfctl_update'],
    }

    file { $conf:
      owner   => '0',
      group   => '0',
      mode    => '0600',
    }

    exec { 'pfctl_update':
      command     => "${pfctl} -nf ${tmpfile} && cp ${tmpfile} ${conf} && ${pfctl} -f ${conf}",
      unless      => "/usr/bin/diff ${tmpfile} ${conf}",
      refreshonly => true,
    }
  } else {
    warning('in order to apply PF rules, you must specify a config template')
  }
}
